'use strict'

exports.task = {
    name: 'pullQGendaData',
    description: 'A cron task to pull data from qgendata api and store in ampcome mongodb everyday',
    frequency: 0,
    schedule: "0 0 12 * * ?",
    queue: 'default',
    middleware: [],

    run: function (api, params, next) {
        console.log('triggered pullQGendaData action');
        /**
         * code comes to here to pull data from qgenda api and push into mongodb
         */
        if (next) { return next(); }
    }
}