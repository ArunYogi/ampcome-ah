var net = require('net');
const delimiter = '\r\n';
var client = new net.Socket({
  writable: true
});
client.setEncoding('utf-8');
client.connect(5000, '127.0.0.1', () => {
  //'connect' listener
  console.log('connected to server!');
  var params = {
    "apiVersion": 1,
    "location": [12.8731461,77.7878538],
    "unit": "arun-yogi-mac"
  };
  var action = 'gpsrecorder';
  var buf = new Buffer.from(JSON.stringify({action, params}) + '\r\n');
  //var buf = Buffer.from("gpsrecorder" + '\r\n');
  console.log('data to be written', buf.toString());
  client.write(buf);
  //client.pipe(client);
});
client.on('data', (data) => {
  var result = String(data);
  console.log('result on data', result);
  if (result.context === 'response') {
    client.end("goodbye" + '\r\n');
  }
});

client.on('end', () => {
  console.log('disconnected from server');
});
