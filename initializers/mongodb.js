'use strict'
const { MongoClient } = require('mongodb');

module.exports = {
    loadPriority: 1000,
    startPriority: 1000,
    stopPriority: 1000,
    initialize: function (api, next) {
        api.mongo = {
            MongoClient: MongoClient,
            connect: function () {
                if (api.config.mongo.startMongo) {
                    var option = { appname: "gpsrecorder" };
                    if (api.config.mongo.debug) {
                        //option.loggerLevel= "debug";
                    }
                    api.mongo.MongoClient.connect(api.config.mongo.connectionURL, option, function (err, db) {
                        if (err) {
                            console.error("Mongodb connection failed", err);
                        }
                        console.log("Connected successfully to server");
                        api.mongo.db = db;
                    });
                }
            },
            disconnect: function (next) {
                if (api.mongo.db) { api.mongo.db.close(); }
                next();
            }
        };
        next();
    },
    start: function (api, next) {
        if (api.config.mongo.enable) {
            api.mongo.connect(next);
        }
    },
    stop: function (api, next) {
        api.mongo.disconnect(next);
    }
};