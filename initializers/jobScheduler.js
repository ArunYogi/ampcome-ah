'use strict'
const nodeSchedule = require('node-schedule')

module.exports = {
    name: 'jobScheduler',
    loadPriority: 1000,
    startPriority: 1000,
    stopPriority: 1000,
    initialize: function (api, next) {
        api.jobScheduler = { scheduledTasks: [] };
        api.scheduledJobs = [];
        return next();
    },
    start: function (api, next) {
        // do this job every 10 seconds, cron style
        const job = nodeSchedule.scheduleJob('0,10,20,30,40,50 * * * * *', () => {
            // we want to ensure that only one instance of this job is scheduled in our environment at once,
            // no matter how many schedulers we have running
            if (api.resque.scheduler && api.resque.scheduler.master) {
                var taskNames = Object.keys(api.tasks.tasks);
                taskNames.forEach(taskName => {
                    var task = api.tasks.tasks[taskName];
                    if (api.jobScheduler.scheduledTasks.findIndex(t => t.name == taskName) == -1) {
                        api.jobScheduler.scheduledTasks.push(task);
                        if (task && task.schedule) {
                            api.scheduledJobs.push(nodeSchedule.scheduleJob(task.schedule, () => {
                                task.run(api, {}, () => {});
                            }));
                        }
                    }
                });
            }
        })

        api.scheduledJobs.push(job);
        return next();
    },
    stop: function (api, next) {
        api.scheduledJobs.forEach((job) => { if (job) job.cancel() });
        return next();
    }
}