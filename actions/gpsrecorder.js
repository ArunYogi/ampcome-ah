'use strict'

exports.action = {
  name: 'gpsrecorder',
  description: 'an actionhero action',
  blockedConnectionTypes: [],
  outputExample: {},
  matchExtensionMimeType: false,
  version: 1.0,
  toDocument: true,
  middleware: [],

  inputs: {
    location: {
      required: true,
      validator: locationValidator,
      formatter: locationFormatter
    },
    unit: {
      required: true
    }
  },

  run: function(api, data, next) {
    var error = null;
    api.log(data.params);
    var now = new Date();
    var recordModel = api.mongo.db.collection('gpsrecords');
    return recordModel.insertOne({geopoint : data.params.location, createdAt : now, unit: data.params.unit})
      .then(function(result) {
        data.response.data = result;
        return next();
      }).catch(function(err) {
        console.error(err);
        return new Error(err);
      });
  }
}

function locationValidator(loc) {
  return (Array.isArray(loc) && loc.length == 2 && (typeof loc[0] === 'number' && typeof loc[1] === 'number'));
}

function locationFormatter(loc) {
  if (typeof loc === 'string') {
    return JSON.parse(loc);
  }
  return loc;
}
